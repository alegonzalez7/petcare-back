const { check, validationResult } = require('express-validator')

const rules = {
    diagnostic : {
        create : [
            check('name')
            .isLength({min:3}).withMessage('Minimo de caracteres minimo es 3')
            .isString().withMessage('El campo enviado no es correcto')
        ],
    },
    owner: {
        form : [
            
            check('name')
            .isLength({min:3})
            .withMessage('Minimo de caracteres minimo es 3')
            .isString()
            .withMessage('El campo enviado no es correcto'),
            
            check('lastname')
            .isLength({min:3})
            .withMessage('Minimo de caracteres minimo es 3')
            .isString()
            .withMessage('El campo enviado no es correcto'),
            
            check('email')
            .isLength({min:3})
            .withMessage('Minimo de caracteres minimo es 3')
            .isString()
            .withMessage('El campo enviado no es correcto')
            .isEmail()
            .withMessage('El email no es valido'),
            
            check('direction')
            .isLength({min:3})
            .withMessage('Minimo de caracteres minimo es 3')
            .isString()
            .withMessage('El campo enviado no es correcto'),
            
            check('document')
            .isLength({min:3})
            .withMessage('Minimo de caracteres minimo es 3')
            .isString()
            .withMessage('El campo enviado no es correcto')
        ]
    },
    pet:{
        form : [
            check('name')
            .isLength({min:3})
            .withMessage('Minimo de caracteres minimo es 3'),
            
            check('age')
            .isInt()
            .withMessage('El campo de edad tiene que ser numero'),
            
            check('raze')
            .isLength({min:3})
            .withMessage('Minimo de caracteres minimo es 3'),
            
            check('specie')
            .isLength({min:3})
            .withMessage('Minimo de caracteres minimo es 3'),
            
            check('comments').
            isLength({min:3})
            .withMessage('Minimo de caracteres minimo es 3'),
            
            check('id_store').notEmpty()
            .withMessage('Seleccione una veterinaria'),
        ]
    },
    store:{
        form:[
            check('name')
            .isEmpty()
            .isLength({min:3}),
            
            check('direction')
            .isEmpty()
            .isLength({min:3})
        ]
    }

}


const validate = request => {
    const errors = validationResult(request)

    return errors
}

module.exports = {
    rules,
    validate
}