const controller = {}
const { Pet, Store } = require('../../models')
const {validate} = require('../validation-request')
controller.getAllPets = async (request, response) => {
    try {
        const pets = await Pet.findAll()
        response.send({
            data: pets
        })
    } catch (error) {
        response.send({
            data: error
        })
    }
}
controller.createPet = async (request, response) => {
    try {
        const errors = validate(request)
        if(errors.isEmpty()){
            const { name, age, raze, comments, id_store, specie } = request.body
        
            const findStore = await Store.findOne({where: {
                id : id_store
            }})
            if(findStore){
                await Pet.create({
                    name : name,
                    age: age,
                    raze:raze,
                    specie:specie,
                    comments: comments,
                    id_store:id_store
                })
                response.send({
                    message: 'Pet created',
                    status:true
                })
            }else{
                
                response.send({
                    message: 'Sorry we didnt find your store',
                    status:false
                })
            }
        }else{
            response.send({
                errors:errors.array(),
                status:false
            })
        }
        
    } catch (error) {
        console.log(error)
        response.send({
            message: error,
            status:false
        })
    }
}
controller.udpatePet = async (request, response) => {
    try {
        const { name, age, raze, comments, store_id } = request.body
        const { id } = request.params
        await Pet.update({
            name : name,
            age: age,
            raze:raze,
            comments: comments,
            store_id: store_id
        }, {
            where: {
                id: id
            }
        })
        response.send({
            message: 'Pet updated',
            status:true
        })
    } catch (error) {
        response.send({
            message: error,
            status:false
        })
    }
}
controller.deletePet = async (request, response) => {
    try {
        const { id } = request.params
        await Pet.destroy({
            where: {
                id : id
            }
        })
        response.send({
            message: 'Pet deleted',
            status:true
        })
    } catch (error) {
        response.send({
            message: error,
            status:false
        })
    }
}
module.exports = controller