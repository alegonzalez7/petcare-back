
const controller = {}
const {Diagnostic} = require('../../models/')
const {validate} = require('../validation-request')
controller.all = async (request, response) => {
    try {
        const diagnostics = await Diagnostic.findAll()
        response.send({
            data : diagnostics
        })
    } catch (error) {
        response.send({
            data : error
        })
    }
}
controller.create = async (request, response) => {
    try {
        const errors = validate(request)
        if(errors.isEmpty()){
            await Diagnostic.create({
                name : request.body.name
            })
            response.send({
                message : "Diagnostic created",
                status:true
            })
        }else{
            response.send({
                errors : errors.array(),
                status:true
            })
        }
    } catch (error) {
        console.log(error)
        response.send({
            message:error,
            status:false
        })
    }
}
controller.delete = async (request, response) => {
    try {
        const {id} = request.params
        await Diagnostic.destroy({
            where:{
                id : id
            }
        })
        response.send({
            message : "Diagnostic deleted",
            status:true
        })
    } catch (error) {
        response.send({
            customMessage:'Identificador no valido',
            message:error,
            status:false
        })
    }
}


module.exports = controller