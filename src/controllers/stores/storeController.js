const controller = {}
const { Store, Pet, Owner, PetsWithOwners } = require('../../models')
const { validate } = require('../../controllers/validation-request')
controller.getAllStores = async (request, response) => {
    try {
        const stores = await Store.findAll()
        response.send({
            data: stores
        })
    } catch (error) {
        response.send({
            data: error
        })
    }
}
controller.createStore = async (request, response) => {
    try {
        const errors = validate(request)
        if(errors.isEmpty()){
            const { name, direction } = request.body
            await Store.create({
                name : name,
                direction : direction
            })
            response.send({
                message: 'Store created',
                status:true
            })
        }else{
            response.send({
                errors: errors.array(),
                status: false,
            })
        }
        
    } catch (error) {
        response.send({
            message: error,
            status:false
        })
    }
}
controller.getPetsByStore = async (request, response) => {
    try {
        const {id} = request.params
        const pets = await Pet.findAll({
            where:{
                id_store : id
            }
        })
        response.send({
            data : pets,
            status : true
        })
    } catch (error) {
        console.log(error)
        response.send({
            data : error,
            status : false
        })
    }
}
controller.getPetsAdopted = async (request, response) => {
    try {
        const petsAdopted = await PetsWithOwners.findAll({
            include:[
                {
                    model: Pet,
                    
                },
                {
                    model: Owner,
                    
                }
            ]
        })
        response.send({
            data:petsAdopted,
            status:true
        })
    } catch (error) {
        console.log(error)
        response.send({
            message : error,
            status : false
        })    
    }
}
controller.deletePetAdopted = async (request, response) => {
    try {
        const { id } = request.params
        await PetsWithOwners.destroy({
            where: {
                id : id
            }
        })
        response.send({
            message: 'Pet deleted',
            status:true
        })
    } catch (error) {
        response.send({
            message: error,
            status:false
        })
    }
}
module.exports = controller