const controller = {}
const { Owner, Pet, PetsWithOwners } = require('../../models')
const { validate } = require('../validation-request')

controller.getAllOwners = async (request, response) => {
    try {
        const owners = await Owner.findAll()
        response.send({
            owners: owners,
            status:true
        })
    } catch (error) {
        response.send({
            data: error
        })
    }
}
controller.createOwner = async (request, response) => {
    try {
        const errors = validate(request)
        if(errors.isEmpty()){
            const {name, lastname, email, direction, document} = request.body
            await Owner.create({
                "name" : name,
                "lastname" : lastname,
                "email": email,
                "direction":direction,
                "document":document
            })
            response.send({
                message: "Owner created",
                status : true
            })
            
        }else{
            response.send({
                errors: errors.array(),
                status : true
            })
        }
    } catch (error) {
        console.log(error)
        response.send({
            message: error,
            status : false
        })
    }
}
controller.updateOwner = async (request, response) => {
    try {
        const errors = validate(request)
        if(errors.isEmpty()){
            const {name, lastname, email, direction, document} = request.body
            const {id} = request.params
            await Owner.update({
                "name" : name,
                "lastname" : lastname,
                "email": email,
                "direction":direction,
                "document":document
            }, {
                where: {
                    id : id
                }
            })
            response.send({
                message: "Owner updated",
                status : true
            })
        }else{
            response.send({
                errors: errors.array(),
                status : true
            })
        }
    } catch (error) {
        response.send({
            message: error,
            status : false
        })
    }
}
controller.deleteOwner = async (request, response) => {
    try {
        const {id} = request.params
        await Owner.destroy({
            where: {
                id : id
            }
        })
        response.send({
            message: "Owner deleted",
            status : true,
        })
    } catch (error) {
        console.log(error)
        response.send({
            message: error,
            status : false
        })
    }
}
controller.adoptPet = async (request, response) => {
    try {
        
        const {id_pet, id_owner} = request.body
        await PetsWithOwners.create({
            id_pet : id_pet,
            id_owner : id_owner
        })
        response.send({
            message: 'Pet adopted',
            status: true
        })
    } catch (error) {
        response.send({
            message: error,
            status: false
        })
    }
}
controller.getMyPets = async (request, response) => {
    try {
        const { id_owner } = request.params
        const petsAdopted = await PetsWithOwners.findAll({
            where:{
                '$PetsWithOwners.id_owner$' : id_owner
            },
            include:[
                {
                    model: Pet,
                    as:'pet'
                },
                {
                    model: Owner,
                    as:'owner'
                    
                }
            ],
            
            
        })
        response.send({
            data:petsAdopted,
            status:true
        })
    } catch (error) {
        console.log(error)
        response.send({
            data : error
        })
    }
}

module.exports = controller