module.exports = {
    database: {
        username: process.env.DB_USERNAME || 'root',
        password: process.env.DB_PASSWORD || '',
        database: process.env.DB_NAME || 'petcare_formacion',
        host: process.env.DB_HOST || 'localhost'
    }
}