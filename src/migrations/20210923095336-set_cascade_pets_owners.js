'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
     try {
      await queryInterface.changeColumn('Pets', 'id_store', {
        type:Sequelize.DataTypes.INTEGER,
          references:{
            model:'Stores',
            key:'id'
          },
          onUpdate:'CASCADE',
          onDelete:'CASCADE'
      })
      await queryInterface.changeColumn('PetsWithOwners', 'id_pet', {
        type:Sequelize.DataTypes.INTEGER,
          references:{
            model:'Pets',
            key:'id'
          },
          onUpdate:'CASCADE',
          onDelete:'CASCADE'
      })
      await queryInterface.changeColumn('PetsWithOwners', 'id_owner', {
        type:Sequelize.DataTypes.INTEGER,
          references:{
            model:'Owners',
            key:'id'
          },
          onUpdate:'CASCADE',
          onDelete:'CASCADE'
      })
      return Promise.resolve();
    } catch (error) {
      return Promise.reject(error);
     }
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
