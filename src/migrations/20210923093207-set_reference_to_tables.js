'use strict';

module.exports = {
  /**
   * @typedef {import('sequelize').Sequelize} Sequelize
   * @typedef {import('sequelize').QueryInterface} QueryInterface
   */

  /**
   * @param {QueryInterface} queryInterface
   * @param {Sequelize} Sequelize
   * @returns
   */
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
     try {
       await queryInterface.changeColumn('PetsWithOwners', 'id_pet', {
        type:Sequelize.DataTypes.INTEGER,
          references:{
            model:'Pets',
            key:'id'
          },
          onUpdate:'CASCADE',
          onDelete:'SET NULL'
      })
      await queryInterface.changeColumn('PetsWithOwners', 'id_owner', {
        type:Sequelize.DataTypes.INTEGER,
          references:{
            model:'Owners',
            key:'id'
          },
          onUpdate:'CASCADE',
          onDelete:'SET NULL'
      })
      return Promise.resolve();
     } catch (error) {
      return Promise.reject(error);
     }
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
