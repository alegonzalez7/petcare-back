const app = require('express')
const router = app.Router()
const ownerController = require('../../controllers/owners/ownersController')
const {rules} = require('../../controllers/validation-request')
router
.get('/', ownerController.getAllOwners)
.post('/', rules.owner.form, ownerController.createOwner)
.put('/:id', rules.owner.form, ownerController.updateOwner)
.delete('/:id', ownerController.deleteOwner)
.get('/:id_owner/pets', ownerController.getMyPets)
.post('/pet/adopt', ownerController.adoptPet)
module.exports = router