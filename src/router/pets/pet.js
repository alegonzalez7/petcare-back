const app = require('express')
const router = app.Router()
const controllerPet = require('../../controllers/pets/petsController')
const {rules} = require('../../controllers/validation-request')
router
.get('/', controllerPet.getAllPets)
.post('/', rules.pet.form, controllerPet.createPet)
.put('/:id', rules.pet.form, controllerPet.udpatePet)
.delete('/:id', controllerPet.deletePet)

module.exports = router
