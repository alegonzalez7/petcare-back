const app = require('express')

const router = app.Router()
const storeController = require('../../controllers/stores/storeController')
const {rules} = require('../../controllers/validation-request')
router
.get('/' , storeController.getAllStores)
.post('/', storeController.createStore)
.get('/:id/pets' , storeController.getPetsByStore)
.get('/pets/adopted' , storeController.getPetsAdopted)
.delete('/:id/adoption', storeController.deletePetAdopted)
module.exports = router