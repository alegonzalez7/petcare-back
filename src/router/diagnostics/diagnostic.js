const app = require('express')
const router = app.Router()
const diagnosticController = require('../../controllers/diagnostics/diagnosticController')
const {rules} = require('../../controllers/validation-request')
router
.get('/all', diagnosticController.all)
.post('/', rules.diagnostic.create ,diagnosticController.create)
.delete('/:id', diagnosticController.delete)

module.exports = router