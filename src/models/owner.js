'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Owner extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Owner.hasMany(models.PetsWithOwners, {
        foreignKey:'id_owner',
        as:'owner'
      })
    }
  };
  Owner.init({
    id:{
      type:DataTypes.STRING,
      autoIncrement:true,
      primaryKey:true,
    },
    name: DataTypes.STRING,
    lastname: DataTypes.STRING,
    email: DataTypes.STRING,
    direction: DataTypes.STRING,
    document: DataTypes.STRING,
    
  }, {
    sequelize,
    modelName: 'Owner',
  });
  return Owner;
};