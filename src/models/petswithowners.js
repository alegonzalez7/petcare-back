'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PetsWithOwners extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      PetsWithOwners.belongsTo(models.Pet, {
        foreignKey : 'id_pet',
        as:'pet'
      })
      PetsWithOwners.belongsTo(models.Owner, {
        foreignKey : 'id_owner',
        as:'owner'
      })
    }
  };
  PetsWithOwners.init({
    id_pet: DataTypes.INTEGER,
    id_owner: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'PetsWithOwners',
  });
  return PetsWithOwners;
};