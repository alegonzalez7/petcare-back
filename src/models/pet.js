'use strict';
/**
 * @return {Sequelize, DataTypes} db//This will make visual code know that "db" is of type Sequelize.Sequelize
 */
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Pet extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Pet.belongsTo(models.Store, {
        foreignKey:'id'
      })
      Pet.hasMany(models.PetsWithOwners, {
        foreignKey:'id_pet'
      })
    }
  };
  Pet.init({
    id:{
      type:DataTypes.STRING,
      autoIncrement:true,
      primaryKey:true,
    },
    name: DataTypes.STRING,
    age: DataTypes.INTEGER,
    raze: DataTypes.STRING,
    specie: DataTypes.STRING,
    comments:DataTypes.STRING,
    id_store:DataTypes.INTEGER,
    specie : DataTypes.STRING,
    
  }, {
    sequelize,
    modelName: 'Pet',
  });
  return Pet;
};