const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const port = process.env.PORT || 5000;
const mysql = require('mysql2')
const app = express()
const sequelize = require('./src/db/db')

//

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended : true
}))
app.use((err, req, res, next) => {
    const statusCode = err.statusCode || 500;
    console.error(err.message, err.stack);
    res.status(statusCode).json({'message': err.message});
    return;
});

//read .env file
var env = require('node-env-file'); // .env file
env(__dirname + '/.env');

//router instance paths
const diagnosticRouting = require('./src/router/diagnostics/diagnostic')
app.use('/diagnostic', diagnosticRouting)

const ownerRouting = require('./src/router/owners/owner');
app.use('/owners', ownerRouting)

const petRouting = require('./src/router/pets/pet');
app.use('/pets', petRouting)

const storeRouting = require('./src/router/stores/store');
app.use('/stores', storeRouting)

app.listen(port, async () => {
    sequelize.authenticate().then(()=> console.log('hey you are connected')).catch((error) => console.log(error))
})